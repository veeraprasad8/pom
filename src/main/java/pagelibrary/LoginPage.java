package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverconfig.TestBase;

public class LoginPage extends TestBase {
	
	WebDriver driver;
	
	@FindBy(xpath = "//h1[text()='Admin area demo']")
	WebElement lableText;
	
	@FindBy(id = "Email")
	WebElement email;
	
	@FindBy(id = "Password")
	WebElement password;
	
	@FindBy(xpath = "//input[@value='Log in']")
	WebElement loginButton;

	@FindBy(xpath = "//div[text()='Login was unsuccessful. Please correct the errors and try again.']")
	WebElement errorMessageLabel;
	
	public LoginPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, LoginPage.class);
		
	}
	
	public boolean lableisDisplayed(){
		
		return lableText.isDisplayed();
	}
	public void loginToApp(String uname, String pwd){
		
		email.clear();
		email.sendKeys(uname);
		password.clear();
		password.sendKeys(pwd);
		loginButton.click();
	}

	public boolean isErrorLableDisplayed(){

		return errorMessageLabel.isDisplayed();
		
	}

	
	
	

}
