package driverconfig;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class TestBase {
	
	public Properties prop;
	public WebDriver driver;
	
	
	@BeforeSuite
	public void loadProperties(){
		
		prop = new Properties();
		try {
		FileInputStream fs = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\java\\properties\\config.properties");
		prop.load(fs);
		}
		
		catch (Exception e){
			
			e.printStackTrace();
		}
		
	}
	
	
	@BeforeTest
	public void loadUrl(){
		
		
		if (prop.get("browser").equals("chrome")){
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		else if (prop.get("browser").equals("firefox")){
			
			driver = new FirefoxDriver();
		}
		
		else {
			System.out.println("please specify the driver");
		}
		
		driver.get(prop.getProperty("appUrl"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}
	

		@AfterMethod
		public void takesScrenShotforFailureTests(ITestResult result )throws Exception{

		if(result.getStatus() == ITestResult.FAILURE){

			// takesscrrenshot
			TakesScreenshot ts =(TakesScreenshot)driver;
			File src = ts.getScreenshotAs(OutputType.FILE);
			FileHandler.copy(src, new File(""));
			}


		}


	
	@AfterTest
	public void closeBrowser(){
		
		driver.quit();
	}
	

}
