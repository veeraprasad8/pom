package utilities;
import driverconfig.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;



public class ApplicationUtilities extends TestBase {

    public void seletDropDownValue(WebElement ele, String valueToSelect){

        Select select = new Select(ele);
        select.selectByVisibleText(valueToSelect);
    }

    public void dragAndDrop(WebElement src, WebElement target){

        Actions act = new Actions(driver);
        act.dragAndDrop(src, target).build().perform();
    }


    public String getElementText(WebElement ele){

        return ele.getText();
    }












}
