package testcases;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pagelibrary.LoginPage;
import driverconfig.TestBase;
import utilities.ExcelUtils;



public class LoginTest extends TestBase {


	String path = "C:\\Users\\Sai Ram\\Documents\\pomwithtestng\\src\\main\\java\\testdata\\data.xlsx";
	String SheetName = "Sheet1";
	
	LoginPage loginpage;
	
	@Test(description = "verifying the login functionality", dataProvider = "data" )
	public void loginVerify(String uname, String passwrod){
		
		loginpage = new LoginPage(driver);
		
		Assert.assertTrue(loginpage.lableisDisplayed());
		
		loginpage.loginToApp(uname, passwrod);
		if(!uname.equals("admin@yourstore.com") && !passwrod.equals("admin")){

			Assert.assertTrue(loginpage.isErrorLableDisplayed());
		}
		else {
			Assert.assertEquals(driver.getTitle(), "Dashboard / nopCommerce administration");

		}

		
	}
	@DataProvider(name = "data")
	public Object[][] dataRead(){
		ExcelUtils Data = new ExcelUtils(path);
		int rowNum = Data.getRowCount(SheetName);
		System.out.println(rowNum);
		int colNum = Data.getColumnCount(SheetName);
		System.out.println(colNum);
		Object sampleData[][] = new Object[rowNum][colNum];
		for (int i = 2; i <=rowNum+1; i++) {
			for (int j = 0; j <colNum; j++) {
				sampleData[i-2][j] = Data.getCellData(SheetName, j, i);
			}
		}
		return sampleData;

	}



}
