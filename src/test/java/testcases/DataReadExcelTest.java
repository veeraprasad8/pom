package testcases;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utilities.ExcelUtils;

public class DataReadExcelTest {

String path = "C:\\Users\\Sai Ram\\Documents\\pomwithtestng\\src\\main\\java\\testdata\\data.xlsx";
String SheetName = "Sheet1";
    @Test(dataProvider = "data")
    public void getDataCell(Object p1, Object p2){

//        ExcelUtils excelUtils = new ExcelUtils(path);
//        System.out.println("row count is : "+excelUtils.getRowCount("Sheet1"));
//        System.out.println(excelUtils.getCellData("Sheet1",1, 2));
        System.out.println(p1);
        System.out.println(p2);
    }




    @DataProvider(name = "data")
    public Object[][] dataRead(){
        ExcelUtils Data = new ExcelUtils(path);
        int rowNum = Data.getRowCount(SheetName);
        System.out.println(rowNum);
        int colNum = Data.getColumnCount(SheetName);
        System.out.println(colNum);
        Object sampleData[][] = new Object[rowNum][colNum];
        for (int i = 2; i <=rowNum+1; i++) {
            for (int j = 0; j <colNum; j++) {
                sampleData[i-2][j] = Data.getCellData(SheetName, j, i);
            }
        }
        return sampleData;

    }



}
